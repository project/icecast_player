ICECAST PLAYER MODULE
=====================

[NOTE: This file contains both PHP & HTML code samples and will not
       display correctly in your browser (ex. via the link under the
       Advanced Help section). Please look at the raw file on the server.]

The Icecast Player module is an easy to use module to provide an embedded player
for Icecast (http://www.icecast.org) and SHOUTcast (http://www.shoutcast.com)
audio streams. It provides a configuration wrapper around the Muses Radio Player
(http://www.musesradioplayer.com) and outputs the player in a block that can
be displayed in a variety of ways. For one example, see below in this file.


Configuration Options
---------------------

Muses Radio Player path:  The location (relative to your base path) of the
                          Muses Radio Player files. It is recommended that
                          you put this in site/all/libraries/muses_player,
                          which is the default, but you can change it here.

           Stream title:  An optional short description of the stream which
                          will be shown in the player's window. Limit of 20
                          characters.

             Stream URL:  The URL of the Icecast/Shoutcast stream you want
                          to play.

   Intro audio file URL:  If provided, the audio file specified here will
                          play prior to the stream as a pre-roll. This could
                          be used for an introductory welcome message, an ad
                          space, etc.

               Autoplay:  If checked, the stream will begin playing auto-
                          matically when loaded after the desired amount of
                          buffer has been filled. If unchecked, the user will
                          have to click the Play button to start the stream.

            Buffer time:  The number of seconds of audio to preload before
                          starting to play the stream. Five seconds seems to
                          work well but a slightly larger setting may be
                          desired depending on your network connection.

            Player skin:  The name of the skin file to be used. This list is
                          automatically generated from XML files in the location
                          specified in the Muses Radio Player path (above). If
                          you change that setting, therefore, you will have to
                          save the changes before the options will be updated.
                          If you don't set one, or if the files of the selected
                          skin can't be read, the default skin will be used.

               Language:  The language used in the player window for informative
                          messages such as Play, Stop, Volume, Intro, Connection
                          Error, etc. Automatic usually works just fine but you
                          may want to specify a particular language here.

         Initial volume:  The volume that the player will be set to when first
                          loaded. Default is 70%.



Example Implementation
----------------------

Icecast Player was written specifically as a "scratch your own itch" module for
our university radio station manager who wanted a live stream player like many
commercial radio stations have. The module simply provides a player in a block
so you have to implement in into your site somehow. While your implementation
certainly needs to fit your needs and your site, I thought it might be helpful
to include a basic write-up of how we implemented it for our station.

Obviously, the most basic way to include this player into your site would be
simply enabling the block on every page of your site but doing this with an
audio player presents some problems. For example, if you have the player on
every page, the audio will rebuffer and restart every time the user navigates to
a new page. This is, of course, a somewhat less-than-desirable user experience. 

By the same token, including the block only on some pages would mean that the
audio would stop whenever the user navigated away from one of those pages. The
other problem with embedding the block in your pages is that, if the user opens
a page in a new window or tab, they could easily end up with multiple active
players and multiple completing audio streams.

That's why most radio stations implement their live stream in a pop-up window.
Users can continue to navigate around the site - or even leave the site -
and the stream keeps playing without any interruption. It's also less likely
that the user will open multiple players this way although it is still possible.

Anyway, here's what we did to implement this module in a pop-up window:

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

1 - Enable and configure the module as detailed in the INSTALL.txt file.


  
  ---- NOTE REGARDING STEPS 2-5: --------------------------------------------
  |                                                                         |
  |   CODING PHP IN NODE CONTENT IS DANGEROUS AND IS GENERALLY CONSIDERED   |
  |   TO BE A PROFOUNDLY BAD IDEA. I TAKE NO RESPONSIBILITY IF YOU SCREW    |
  |       SOMETHING UP AND BREAK YOUR SITE. PROCEED AT YOUR OWN RISK.       |
  |                                                                         |
  |   That said, it has been pointed out that the same thing could be       |
  |   accomplished with a page callback in hook_menu but I haven't had      |
  |   time to really get this working yet. If anyone would like to work     |
  |   that up and submit a patch, I'd be more than happy to review it       |
  |   and add it into the module.                                           |
  |                                                                         |
  ---------------------------------------------------------------------------



2 - Instead of enabling the block in your theme(s), create a simple page node
    with an input format of PHP and the following content:

    - - - - - - - - - -

    <?php
      $block = module_invoke('icecast_player', 'block', 'view', 0);
      print $block['content'];
    ?>

    - - - - - - - - - -

    This will give you a page with the block in the content area.

3 - Set the path of the node to something meaningful and publish it.

4 - Make a copy of page.tpl.php in your theme directory and name it
    page-node-{NODEID}.tpl.php where {NODEID} is the node id of the page
    you just created.

5 - Edit that template file and remove anything you don't want to include.
    For our implementation, we removed almost everything (header, footer,
    links, scripts, sidebars, etc). We left the whole nested DIV structure
    in place so the designer would have a nice playground to work in to
    make it look pretty. It ended up looking like this:

    - - - - - - - - - -

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

    <head>
      <title><?php print $head_title; ?></title>
    </head>
    <body class="<?php print $body_classes; ?>">

    <div id="page-wrapper">

      <div id="page"><div id="page-inner">

        <div id="main"><div id="main-inner" class="clear-block">

          <div id="content"><div id="content-inner">

            <div id="content-area">
              <?php print $content; ?>
            </div>

          </div></div> <!-- /#content-inner, /#content -->

        </div></div> <!-- /#main-inner, /#main -->

      </div></div> <!-- /#page-inner, /#page -->

    </div> <!-- /#page-wrapper -->

    </body>
    </html>

    - - - - - - - - - -

6 - Do all the design type stuff to wrap the player block with whatever you
    like.

7 - Create a block with the following content:

    - - - - - - - - - -

    <script type="text/javascript">
    function playerPopup()
    {
        playerWindow = window.open("{PLAYER URL}", "{WINDOW NAME}", "status=0,toolbar=0,location=0,menubar=0,directories=0,resizable=0,scrollbars=0,height=200,width=400");
        playerWindow.moveTo(0, 0);
    }
    </script>
    <a href="#" onClick="playerPopup(); return false;">{LINK CONTENT}</a>

    - - - - - - - - - -

    You need to set {PLAYER_URL} to the path you gave the node that pulled in
    the block in step 3. {WINDOW NAME} is arbitrary and is only used if you
    want to address the window wth javascript. We don't so it doesn't matter.
    Your {LINK CONTENT} can be whatever. Text, image, big mixture of stuff.
    For us, it's an image with a big "Listen Live" button.

    Change the window dimensions in the window.open() function to be the size
    you want for your pop-up player window. This should, obviously, be large
    enough to accomodate whatever skin you choose.

    You can have the javascript anywhere - and it's probably more correct to
    include it in your site using druapl_add_js() somewhere - but this is the
    only place we do this on our site and it didn't seem worth the trouble for
    such a small hit. This also lets us easily edit the script if we need to
    without all the fuss. As always, YMMV. Do it however you want.

8 - Enable that block on any pages on which you want the link to appear and
    have at it.  Clicking the link should open the pop-up with the player.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
