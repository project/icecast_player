<?php

/**
 * @file
 * This is the Icecast Player admin include which provides an interface
 * to Icecast Player to change some of the default settings
 */


/**
 * Function to generate the form setting array.
 */
function icecast_player_settings_form() {
  // Get the settings.
  $settings = _icecast_player_get_settings();

  $form['library_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Library Information'),
    '#collapsible' => FALSE,
  );

  $form['library_info']['icecast_player_library_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Muses Radio Player path'),
    '#default_value' => $settings['icecast_player_library_path'],
    '#description' => t('Enter the location of the !download. It is recommended to use e.g. sites/all/libraries/muses_player.', array(
      '!download' => l(t('Muses Radio Player'), 'http://www.musesradioplayer.com/download.php', array('absolute' => TRUE)),
    )),
  );

  $form['stream_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stream Information'),
    '#collapsible' => FALSE,
  );

  $form['stream_info']['icecast_player_stream_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Stream title'),
    '#description' => t("A human readable description of the stream to show in the player. (20 character limit)"),
    '#default_value' => $settings['icecast_player_stream_title'],
    '#size' => 40,
    '#maxlength' => 20,
  );

  $form['stream_info']['icecast_player_stream_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Stream URL'),
    '#description' => t("The URL of the audio stream you want the player to play."),
    '#default_value' => $settings['icecast_player_stream_url'],
    '#required' => TRUE,
    '#size' => 80,
  );

  $form['stream_info']['icecast_player_intro_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Intro audio file URL'),
    '#description' => t("The URL of the audio file to play as an introduction before starting to play the main audio stream."),
    '#default_value' => $settings['icecast_player_intro_url'],
    '#size' => 80,
  );

  $form['player_info']['icecast_player_stream_codec'] = array(
    '#type' => 'select',
    '#title' => t('Audio codec'),
    '#options' => drupal_map_assoc(_icecast_player_get_codecs()),
    '#description' => t("The audio codec of the stream."),
    '#default_value' => $settings['icecast_player_stream_codec'],
  );

  $form['stream_info']['icecast_player_autoplay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoplay'),
    '#description' => t('If enabled, the stream will begin playing automatically when the player is loaded.'),
    '#default_value' => $settings['icecast_player_autoplay'],
  );

  $form['stream_info']['icecast_player_buffer_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Buffer time'),
    '#description' => t("The amount of time (in seconds) to buffer the audio prior to playing."),
    '#default_value' => $settings['icecast_player_buffer_time'],
    '#size' => 4,
    '#maxlength' => 4,
  );

  $form['player_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Player Configuration'),
    '#collapsible' => FALSE,
  );

  $form['player_info']['icecast_player_skin_xml_file'] = array(
    '#type' => 'select',
    '#title' => t('Player skin'),
    '#options' => drupal_map_assoc(_icecast_player_get_skin_files($settings['icecast_player_library_path'])),
    '#description' => t("The URL of the audio stream you want the player to play."),
    '#default_value' => $settings['icecast_player_skin_xml_file'],
  );

  $form['player_info']['icecast_player_language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => drupal_map_assoc(_icecast_player_get_languages()),
    '#description' => t("The language to show in the player."),
    '#default_value' => $settings['icecast_player_language'],
  );

  $form['player_info']['icecast_player_initial_volume'] = array(
    '#type' => 'textfield',
    '#title' => t('Initial volume'),
    '#description' => t("Initial position for the volume slider when the payer first loads."),
    '#default_value' => $settings['icecast_player_initial_volume'],
    '#size' => 4,
    '#maxlength' => 3,
    '#field_suffix' => '%',
  );

  return system_settings_form($form);
}
